const csv = require("csv-parser");
const fs = require("fs");
const audioJson = {};

const args = process.argv;
const filePath = args[2];

if (!filePath) {
  console.log("Usage: npm run createAudioJson <path to audios csv>")
} else {
  fs.createReadStream(filePath)
  .on("error", err => console.log("Could not locate " + filePath))
  .pipe(csv())
  .on("data", data => {
    
    for (key of Object.keys(data)) {
      data[key.trim()] = data[key];
      if (key !== key.trim()) 
        delete data[key];
    }
  
    audioJson[data["filename"]] = {
      name: data["filename"],
      filename: data["filename"] + ".mp3",
      src: data["filename"] + ".mp3",
      duration: 0,
      audioState: "NOT_DETERMINED",
      caption: data.text
    };
  })
  .on("end", () => {
    fs.writeFile("web-app/data/audioORIGINAL.json", JSON.stringify({
      "en_watson": audioJson
    }, null, 2), err => {
      if (err) throw err;
      console.log("Created audioORIGINAL.json in web-app/data/audioORIGINAL.json");
    });
  });
}