# Create Audio Json

This generates a new audio.json (web-app/data/audioORIGINAL.json) from the rnl_assets/PreETL/all_audios.csv file obtained using the Implementation Template parser.

## Usage

### Installation
```
npm install create-audio-json --save-dev
```

### Scripts

Update package scripts for running scripts.
Add to package.json:
```
"createAudioJson": "node node_modules/create-audio-json/src/createAudioOriginal"
```

### Running

To create audioORIGINAL.json:
```
npm run createAudioJson <path to an audio csv file>
```